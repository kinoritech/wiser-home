# Wiser-home
Dryton Wiser Smart Radiator Thermostat Homeassistant integration

This project provides a Homeassistant (HA) component and lovelavce card that allows people to setup a smart thermostat
solution for their house using Dryton Wiser's Smart Radiator Thermostats (TRVs).

The HA component allows configuring the different rooms in a house (multiple TRVs per room) and setting the TRVs
schedule. The HA lovelace card (work in progress) allows to view the state of the valves and control them.

## Prerequisites

In order to use the TRVs via the provided component they must be added to HA via zigbee2mqtt integration. If you
don't know what I am talking about, head to the [zigbee2mqtt](https://www.zigbee2mqtt.io) site and learn what you need
to do to make it work. I have contributed the TRVs zigbee2mqtt [integration](https://www.zigbee2mqtt.io/devices/WV704R0A0902.html),
so they should work out of the box.

This implementation assumes you have a switch configured on HA that can be used to turn on the boiler. 

## Installing the wiser-home components

There are two parts to this integration the HA component and the lovelace card. Download or copy this repo to your
PC/other. Unzip it in your preferred location. I will use (windows) `c:\temp\wiser-home` for the rest of the instruction. 

### For the component
- Locate the HA installation in your PC, raspberry pi, etc. By installation I mean the folder where you run it from (in 
my case it is a python virtual environment). For example: `D:\HA\.virtualenvs\homeassistant-x32ZykST\Lib\site-packages\homeassistant`.
- Copy the component to the HA components folder:
```
Xcopy /E /I /s c:\temp\wiser-home\component\wiser_home D:\HA\.virtualenvs\homeassistant-x32ZykST\Lib\site-packages\homeassistant\components\wiser_home
```

### For the lovelave card
- Locate the location of your HA configuration files. This is where you add your various YAML files (configuration.yaml, 
sensor.yaml, etc.)
copy the js file to HA's www folder:
```
Xcopy /E /I /s c:\temp\wiser-home\www D:\HA\config\
```

**Note:** You don't need the lovelave card for the HA component to work, is just a useful way of monitoring the TRVs.

### Enable

You need to restart your HA service so the new component is loaded (only restarting your lovelace frontend is not
enough.)

## Using the wiser-home component

The component must be added to HA as a sensor (e.g. to your `senor.yaml` file.). The component allows you to configure
the rooms in your house. Each room can have 1 or more TRVs. Each room has its own schedule. The structure is as follows
```yaml
- platform: wiser_home
    boiler: <boiler switch id>
    rooms:
      <room_name>:
        thermostat:
          - entity_id: <HA TRV id>
          - entity_id: <HA TRV id>
        schedule:
          - v: 16
``` 
- You need to provide the boiler switch id
- Each room must have a name.
- Each room can use one or more thermostats, each identified by its sensor id
- Each room must provide a schedule. 

For example, this is an excerpt of mine:

```yaml
- platform: wiser_home
    boiler: switch.el_switch_1_switch
    rooms:
      master:
        thermostat:
          - entity_id: climate.dw_valve_3_climate
        schedule:
          # The bedroom should always have 16 degrees to sleep well in there.
          - v: 16
      office:
        thermostat:
          - entity_id: climate.dw_valve_2_climate
        schedule:
          - v: 20
            rules:
              - weekdays: 1-5,7
                rules:
                  - { start: "06:30", end: "13:00" }
          - v: 18
            rules:
              - weekdays: 1-5,7
                rules:
                  - { start: "13:00", end: "18:30" }
          - v: 16
            rules:
              - weekdays: 1-5,7
                rules:
                  - { start: "18:30", end: "21:00" }
              - weekdays: 6
                rules:
                  - { start: "16:00", end: "18:00" }
```

### The Schedule
The schedule use the same format as [Schedy](https://hass-apps.readthedocs.io/en/stable/apps/schedy/); please refer to
their excellent online documentation to lear how to create schedules. [efficiosoft](https://github.com/efficiosoft) has
done a great job with them! 

Currently, temperatures can only be specified in °C.

## Using the lovelave card

Just go to your overview page, select a target tab and add a new **Manual Card**. When prompted, provide the correct
type: `type: 'custom:wiser-home-card'`.  
After you have added the card, edit it and select your `wiser_home` component to monitor.

**Note:** The lovelave card is work in progress, so currently it only displays the room information (temp, setpoint, 
heat demand). 

## Services

The HA component currently offers two services in order to enable and configure away mode:
- Away mode
- Away temp

The away mode service, `wiser_home.set_away_mode`, allows you to Activate/desactivate (true/false) the away mode. When
*active*, the component ignores the schedule of the rooms and sets all valves to the away temp, by default its 16°C.

The away temp service, `wiser_home.set_away_temp`, allows you to change the away temperature. 

You can invoke the services via the UI or, for example, you could use a location automation to set away mode when
everyone leaves the house. 


## Contributing to wiser home

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

## Contact

If you want to contact me you can reach me at `hacker <dot> team <at> kinori <dot> tech`. 

## License

This project uses the following license: [MIT Licence](https://en.wikipedia.org/wiki/MIT_License).